module periph_temp(clk , rst , d_in , cs , addr , rd , wr, d_out, i2c_sda, i2c_scl );
	
	input clk;
	input rst;
	input [15:0]d_in;
	input cs;
	input [3:0]addr; 
	input rd;
	input wr;
	output reg [15:0]d_out;

	inout i2c_sda;
	inout i2c_scl;
      
	wire ready;
	wire i2c_done;	
	wire[1:0] sensornum;	
	wire[15:0] rtc;
	wire[15:0] temp;
	wire ack_error;
	wire tmp_clk;
	
	wire[15:0] temp0;//Cables que muestran las salidas del I2C
	wire[15:0] temp1;
	wire[15:0] temp2;
	wire[15:0] temp3;
	

	reg enable;
	
	
	reg[3:0]s;
	
	always @(*) begin//------address_decoder------------------------------
	
	  case(addr)
	    4'h0:begin s=(cs&&rd)?4'b0000:4'b1110;end //temp0
	    4'h2:begin s=(cs&&rd)?4'b0010:4'b1110;end //temp1
	    4'h4:begin s=(cs&&rd)?4'b0100:4'b1110;end //temp2
	    4'h6:begin s=(cs&&rd)?4'b0110:4'b1110;end //temp3
	    4'h8:begin s=(cs&&rd)?4'b1010:4'b1110;end //rtc
	    4'hA:begin s=(cs&&wr)?4'b1111:4'b1110;end //enable
	    4'hC:begin s=(cs&&rd)?4'b1000:4'b1110;end //ack_error
	    4'hE:begin s=(cs&&rd)?4'b1100:4'b1110;end //data_done
	    default: begin s=4'b1110; end
	  endcase
	end
	
	always @(negedge clk) begin//-------------------- escritura de registros 

	  enable   = (s[0]) ? d_in[0] : enable;

	end
	
	always @(negedge clk) begin//-----------  multiplexa salidas del periferico
	  case (s[3:1])
	    3'b000: d_out = temp0 ;
	    3'b001: d_out = temp1 ;
	    3'b010: d_out = temp2 ;
	    3'b011: d_out = temp3 ;
	    3'b100: d_out = ack_error;
	    3'b101: d_out = rtc ;   
	    3'b110: d_out = i2c_done ;  
	    default: d_out = 16'hff ;
	  endcase
	end
		

	I2C i2c(.clk_50(clk),
		.sda(i2c_sda),
		.scl(i2c_scl),
		.rtc_out(rtc),
		.temp_out(temp),
		.sensornum(sensornum),
		.ack_error(ack_error),
		.enable(enable),
		.reset(rst),
		.done(i2c_done),
		.TMP_CLK(tmp_clk));
		
	Cycle_Sens cycle_s(.clk_50(clk),
		      .data_done(i2c_done),
		      .TMP_CLK(tmp_clk),
		      .sensornum(sensornum),
		      .temp0(temp0),
		      .temp1(temp1),
		      .temp2(temp2),
		      .temp3(temp3),
		      .temp_in(temp));	
		      
		      
endmodule


	
