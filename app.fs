\ Instrucciones de las funciones:
\ (bottom) ... (top: ultimo ingresado)

: delay_1ms
	d# 4167 begin d# 1 - dup d# 0 = until drop
;

\ (Tiempo en ms)
: delay_ms
	d# 0 do delay_1ms loop
;

\ (Valor de rojo) (Numero de led: 1-35)
: everloopSetRed
	d# 1 - dup dup dup + + +
	ever_start + d# 1 + !
;

\ (Valor de verde) (Numero de led: 1-35)
: everloopSetGreen
	d# 1 - dup dup dup + + +
	ever_start + !
;

\ (Valor de azul) (Numero de led: 1-35)
: everloopSetBlue
	d# 1 - dup dup dup + + +
	ever_start + d# 2 + !
;

\ (Valor de blanco) (Numero de led: 1-35)
: everloopSetWhite
	d# 1 - dup dup dup + + +
	ever_start + d# 3 + !
;

\ (Rojo) (Verde) (Azul) (Blanco) (Numero de led: 1-35)
: everloopSetRGBW
	swap over everloopSetWhite
	swap over everloopSetBlue
	swap over everloopSetGreen
	everloopSetRed
;

\ (Rojo) (Verde) (Azul) (Numero de led: 1-35)
: everloopSetRGB
	d# 0 over everloopSetWhite
	swap over everloopSetBlue
	swap over everloopSetGreen
	everloopSetRed
;

\ Apaga todos los LEDs
: everloopClear
	d# 0 ever_start
	begin
		over over !
		d# 1 + dup
		ever_end =
	until
	drop drop
;

\ UART: nueva linea
: uart-endl
	d# 13 emit-uart
	d# 10 emit-uart
;

variable unidades
variable decenas
variable centenas

\ Imprime numero de 0 a 999 a string
: printNum
	d# 0 unidades c!
	d# 0 decenas c!
	d# 0 centenas c!
	dup d# 1000 <
	swap dup d# 0 <>
	rot and if
		d# 100 +
		begin 
			centenas c@ 1+ centenas c!
			d# 100 - dup d# 100 < 
		until
		d# 10 +
		begin 
			decenas c@ 1+ decenas c!
			d# 10 - dup d# 10 < 
		until
		unidades c!
		centenas c@ 1- centenas c!
		decenas c@ 1- decenas c!
	then
	centenas c@ d# 48 + emit-uart
	decenas c@ d# 48 + emit-uart
	unidades c@ d# 48 + emit-uart
;

variable demoR
variable demoG
variable demoB
variable demoLed
variable demoColor
variable lineaPunto
variable flagReiniciar
variable temp1r
variable temp2r
variable temp3r
variable temp0r



\ Dibuja linea
: drawLine
	d# 1 +
	d# 1 demoLed c!
	begin
		demoR c@ demoG c@ demoB c@ demoLed c@ everloopSetRGB
		dup
		demoLed c@ d# 1 + dup demoLed c!
		=
	until
	drop
;

\ Dibuja estado de Sensores (Prueba)
\  (tempRef)(temperatura) (#Sensor) drawState
\ : drawState
\	d# 1 demoLed c!
\	dup 0<> if
\		begin
\		demoLed c@ d# 8 + demoLed c!
\		d# 1 -
\		dup 0=
\		until
\	then	
\
\	drop
\
\	dup h# ffff = if
\
\		d# 7
\		begin
\		d# 20 d# 0 d# 0 demoLed c@ everloopSetRGB
\		demoLed c@ d# 1 + demoLed c!
\		d# 1 -
\		dup 0=
\		until
\		drop
\	else
\		2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ < if
			
			
\ Dibuja estado de Sensores 
: drawState



	d# 1 demoLed c!
	temp0 @ h# ffff = if \ No funcional
		d# 0 refrig0 !
		d# 0 calef0 !
		d# 1 alarm0 !
		begin
		d# 20 d# 0 d# 0 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 9 = 
		until
	else

		temp0 @ d# 8 rshift temp0r c@ d# 1 - < if  \ Frio
			d# 0 refrig0 !
			d# 1 calef0 !
			d# 0 alarm0 !
			begin
			d# 0 d# 7 d# 12 demoLed c@ everloopSetRGB
			demoLed c@ d# 1 + dup demoLed c!
			d# 9 = 
			until
		else
			temp0 @ d# 8 rshift temp0r c@ d# 1 + > if \ Caliente
				d# 1 refrig0 !
				d# 0 calef0 !
				d# 0 alarm0 !
				begin
				d# 25 d# 7 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 9 = 
				until
			else			\ Normal
				d# 0 refrig0 !
				d# 0 calef0 !
				d# 0 alarm0 !
				begin
				d# 0 d# 20 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 9 = 
				until
			then
		then
	then


	temp1 @ h# ffff = if
		d# 0 refrig1 !
		d# 0 calef1 !
		d# 1 alarm1 !
		begin
		d# 20 d# 0 d# 0 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 17 = 
		until
	else

		temp1 @ d# 8 rshift temp1r c@ d# 1 - < if \ Frio
			d# 0 refrig1 !
			d# 1 calef1 !
			d# 0 alarm1 !
			begin
			d# 0 d# 7 d# 12 demoLed c@ everloopSetRGB
			demoLed c@ d# 1 + dup demoLed c!
			d# 17 = 
			until
		else
			temp1 @ d# 8 rshift temp1r c@ d# 1 + > if \ Caliente
				d# 1 refrig1 !
				d# 0 calef1 !
				d# 0 alarm1 !
				begin
				d# 25 d# 7 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 17 = 
				until
			else
				d# 0 refrig1 !                     \ Normal
				d# 0 calef1 !
				d# 0 alarm1 !
				begin
				d# 0 d# 20 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 17 = 
				until
			then
		then
	then


	temp2 @ h# ffff = if
		d# 0 refrig2 !
		d# 0 calef2 !
		d# 1 alarm2 !
		begin
		d# 20 d# 0 d# 0 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 25 = 
		until
	else

		temp2 @ d# 8 rshift temp2r c@ d# 1 - < if \ Frio
			d# 0 refrig2 !
			d# 1 calef2 !
			d# 0 alarm2 !
			begin
			d# 0 d# 7 d# 12 demoLed c@ everloopSetRGB
			demoLed c@ d# 1 + dup demoLed c!
			d# 25 = 
			until
		else
			temp2 @ d# 8 rshift temp2r c@ d# 1 + > if \ Caliente
				d# 1 refrig2 !
				d# 0 calef2 !
				d# 0 alarm2 !
				begin
				d# 25 d# 7 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 25 = 
				until
			else
				d# 0 refrig2 !
				d# 0 calef2 !
				d# 0 alarm2 !
				begin
				d# 0 d# 20 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 25 = 
				until
			then
		then
	then



	temp3 @ h# ffff = if
		d# 0 refrig3 !
		d# 0 calef3 !
		d# 1 alarm3 !
		begin
		d# 20 d# 0 d# 0 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 33 = 
		until
	else

		temp3 @ d# 8 rshift temp3r c@ d# 1 - < if \ Frio
			d# 0 refrig3 !
			d# 1 calef3 !
			d# 0 alarm3 !
			begin
			d# 0 d# 7 d# 12 demoLed c@ everloopSetRGB
			demoLed c@ d# 1 + dup demoLed c!
			d# 33 = 
			until
		else
			temp3 @ d# 8 rshift temp3r c@ d# 1 + > if \ Caliente
				d# 1 refrig3 !
				d# 0 calef3 !
				d# 0 alarm3 !
				begin
				d# 25 d# 7 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 33 = 
				until
			else
				d# 0 refrig3 !
				d# 0 calef3 !
				d# 0 alarm3 !
				begin
				d# 0 d# 20 d# 0 demoLed c@ everloopSetRGB
				demoLed c@ d# 1 + dup demoLed c!
				d# 33 = 
				until
			then
		then
	then

	demoColor c@ d# 1 = if
		begin
		d# 10 d# 10 d# 10 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 36 =
		until
		d# 0 demoColor c!
	else
		begin
		d# 0 d# 0 d# 0 demoLed c@ everloopSetRGB
		demoLed c@ d# 1 + dup demoLed c!
		d# 36 =
		until
		d# 1 demoColor c!

	then


;

: main
	begin
		d# 500 delay_ms
		uart_datarcv @ drop \ Limpia buffer de entrada
		everloopClear
		d# 1 demoColor c!
		d# 22 temp0r c!
		d# 22 temp1r c!
		d# 22 temp2r c!
		d# 22 temp3r c!
		d# 0 calef0 !
		d# 0 refrig0 !
		d# 0 alarm0 !
		d# 0 calef1 !
		d# 0 refrig1 !
		d# 0 alarm1 !
		d# 0 calef2 !
		d# 0 refrig2 !
		d# 0 alarm2 !
		d# 0 calef3 !
		d# 0 refrig3 !
		d# 0 alarm3 !

		
		begin
			s" Presione una tecla para iniciar..." type-uart uart-endl

			d# 1 demoLed c!
			demoColor c@ d# 1 and d# 1 = if
				d# 3 demoR c!
			else
				d# 0 demoR c!
			then
			demoColor c@ 2/ d# 1 and d# 1 = if
				d# 3 demoG c!
			else
				d# 0 demoG c!
			then
			demoColor c@ 2/ 2/ d# 1 and d# 1 = if
				d# 3 demoB c!
			else
				d# 0 demoB c!
			then

			begin
				demoR c@ demoG c@ demoB c@ demoLed c@ everloopSetRGB
				demoR c@ 0<> if
					demoR c@ d# 7 + demoR c!
				then
				demoG c@ 0<> if
					demoG c@ d# 7 + demoG c!
				then
				demoB c@ 0<> if
					demoB c@ d# 7 + demoB c!
				then
				d# 30 delay_ms
				demoLed c@ d# 1 + dup demoLed c!
				d# 36 =
			until

			d# 1 demoLed c!

			begin
				d# 0 d# 0 d# 0 demoLed c@ everloopSetRGB
				d# 30 delay_ms
				demoLed c@ d# 1 + dup demoLed c!
				d# 36 =
			until

			demoColor c@ d# 1 + 
			dup d# 8 = if 
				drop d# 1 
			then
			demoColor c!

			uart_flagrcv @ d# 1 =
			
		until

		s" Ha presionado la tecla " type-uart
		uart_datarcv @ emit-uart uart-endl
		d# 2000 delay_ms
		d# 10 demoR c!
		d# 0 demoG c!
		d# 0 demoB c!
		d# 0 lineaPunto c!
		d# 0 flagReiniciar c!

		begin
			d# 200 delay_ms
			d# 25 d# 0 do uart-endl loop
			d# 1 enable ! 
			s" RTC: " type-uart
			rtc @ h# ffff <> if
			rtc @ d# 8 rshift printNum s" :" type-uart rtc @ h# ff and printNum   uart-endl
			else
			s" N/A" type-uart uart-endl
			then
			s" Leyendo sensor " type-uart uart-endl
			
			begin done @ d# 1 = until
			s" Valor del sensor: " type-uart uart-endl
			s" Sensor0: " type-uart
			temp0 @ 2/ 2/ 2/ 2/ h# fff <> if
			temp0 @ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ printNum s" °C" type-uart uart-endl
			else
			s" N/A" type-uart uart-endl
			then
			s" Sensor1: " type-uart
			temp1 @ 2/ 2/ 2/ 2/ h# fff <> if
			temp1 @ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ printNum s" °C" type-uart uart-endl
			else
			s" N/A " type-uart uart-endl
			then
			s" Sensor2: " type-uart
			temp2 @ 2/ 2/ 2/ 2/ h# fff <> if
			temp2 @ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ printNum s" °C" type-uart uart-endl
			else
			s" N/A" type-uart uart-endl
			then
			s" Sensor3: " type-uart
			temp3 @ 2/ 2/ 2/ 2/ h# fff <> if
			temp3 @ 2/ 2/ 2/ 2/ 2/ 2/ 2/ 2/ printNum s" °C" type-uart uart-endl
			else
			s" N/A" type-uart uart-endl
			then			

			s" Temperatura Referencia 0=  " type-uart temp0r c@ printNum s"   -->  q:+ a:-" type-uart uart-endl
			s" Temperatura Referencia 1=  " type-uart temp1r c@ printNum s"   -->  w:+ s:-" type-uart uart-endl
			s" Temperatura Referencia 2=  " type-uart temp2r c@ printNum s"   -->  e:+ d:-" type-uart uart-endl
			s" Temperatura Referencia 3=  " type-uart temp3r c@ printNum s"   -->  r:+ f:-" type-uart uart-endl
			s" x:reiniciar." type-uart uart-endl

			\ Si se presiona una tecla
			uart_flagrcv @ d# 1 = if
				uart_datarcv @

				dup d# 113 = if 			\ q
					temp0r c@ d# 1 + temp0r c!
				then
				dup d# 119 =  if 			\ w
					temp1r c@ d# 1 + temp1r c!
				then
				dup d# 101 = if 			\ e
					temp2r c@ d# 1 + temp2r c!
				then
				dup d# 114 = if 			\ r
					temp3r c@ d# 1 + temp3r c!
				then
					

				dup d# 97 =  if 			\ a
					temp0r c@ d# 1 - temp0r c!
				then
				dup d# 115 = if 			\ s
					temp1r c@ d# 1 - temp1r c!
				then
				dup d# 100 = if 			\ d
					temp2r c@ d# 1 - temp2r c!
				then
				dup d# 102 = if 			\ f
					temp3r c@ d# 1 - temp3r c!
				then

				

				dup d# 120 = if \ x
					d# 1 flagReiniciar c!
				then

			then
			drop

			drawState

			flagReiniciar c@ 0<>
		until

	again
;